#steps:
#1 create a certs volume
docker-compose -f create-certs.yml run --rm create_certs

#2 docker volume inspect
docker volume inspect test-certs_certs |grep Mountpoint

#3 copy the created files out of the mountpoint to wherever you need

#4 if you need to convert any certs/keys to p12 files or java keystores, use scripts in tools folder
