#!/bin/bash
# $1 is the pkcs12 file to inspect
openssl pkcs12 -info -in $1
