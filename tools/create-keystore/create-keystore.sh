#!/bin/bash

# this script takes a node's .crt and .key file, converts it to .p12 and then imports the .p12 into a java keystore
# fill out the config file then run the script
# you will be prompted for passwords

source config
openssl pkcs12 -export -in $CERTFILE -inkey $KEYFILE -name $NAME -out $P12FILE
keytool -importkeystore -destkeystore $KEYSTORE -srckeystore $P12FILE -srcstoretype PKCS12 -keyalg RSA
