#!/bin/bash

# this script takes a ca cert and imports into a java truststore
# fill out the config file then run the script
# you will be prompted for passwords

source config
keytool -import -alias $NAME -trustcacerts -file $CACERTFILE -keystore $TRUSTSTORE -keyalg RSA
